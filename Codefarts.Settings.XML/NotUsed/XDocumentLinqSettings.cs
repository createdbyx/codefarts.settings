﻿/*
<copyright>
  Copyright (c) 2012, Dean Lunz
  All rights reserved.
  deanlunz@createdbyx.com
  http://www.createdbyx.com
</copyright>
*/
namespace CBX.Settings.XML
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    /// <summary>
    /// Provides a XML file based <see cref="ISettings"/> repository.
    /// </summary>
    public class XDocumentLinqSettings : ISettings
    {
        #region Fields

        /// <summary>
        /// The data store.
        /// </summary>
        private Dictionary<string, string> dataStore = new Dictionary<string, string>();

        /// <summary>
        /// The last write time.
        /// </summary>
        private DateTime lastWriteTime = DateTime.MinValue;

        private DateTime lastReadTime = DateTime.MinValue;

        /// <summary>
        /// Gets an array of setting names.
        /// </summary>
        /// <returns>Returns an array of setting names.</returns>
        public string[] GetSettingNames()
        {
            this.Read();
            return this.dataStore.Keys.ToArray();
        }

        public event EventHandler<SettingChangedEventArgs> SettingChanged;
        private SettingChangedEventArgs eventArgs = new SettingChangedEventArgs();

        private int readDelayInSeconds;


        public int ReadDelayInSeconds
        {
            get
            {
                return this.readDelayInSeconds;
            }
            set
            {
                this.readDelayInSeconds = Math.Max(0, value);
            }
        }

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="XDocumentLinqSettings"/> class.
        /// </summary>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public XDocumentLinqSettings(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException("fileName");
            }

            if (Path.GetDirectoryName(fileName).IndexOfAny(Path.GetInvalidPathChars()) != -1)
            {
                throw new Exception("Invalid path characters detected!");
            }

            if (Path.GetFileName(fileName).IndexOfAny(Path.GetInvalidFileNameChars()) != -1)
            {
                throw new Exception("Invalid filename characters detected!");
            }

            this.readDelayInSeconds = 5;
            this.FileName = fileName;

            this.Read();
        }

        ~XDocumentLinqSettings()
        {
            this.Write();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the file name.
        /// </summary>
        public string FileName { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get setting.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        /// <exception cref="ArgumentException">
        /// </exception>
        public T GetSetting<T>(string name)
        {
            Type type = typeof(T);
            //if (type != typeof(string))
            //{
            //    throw new ArgumentException("Generic type T must be of type string.");
            //}

            this.Read();
            return (T)Convert.ChangeType(this.dataStore[name], type); 
        }

        /// <summary>
        /// Returns true if a setting with the specified name exists.
        /// </summary>
        /// <param name="name">The name of the setting to check for.</param>
        /// <returns>Returns true if a setting with the specified name exists.</returns>
        /// <remarks><see cref="name"/> is case sensitive.</remarks>
        public bool HasSetting(string name)
        {
            this.Read();
            return this.dataStore.ContainsKey(name);
        }

        public void RemoveSetting(string name)
        {
            this.dataStore.Remove(name);
            this.Write();
        }

        /// <summary>
        /// The set setting.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <exception cref="ArgumentException">
        /// </exception>
        public void SetSetting(string name, object value)
        {
            //if (!(value is string))
            //{
            //    throw new ArgumentException("'value' argument must be of type string.");
            //}

            if (!this.dataStore.ContainsKey(name))
            {
                this.dataStore.Add(name, null);
            }

            this.dataStore[name] = value.ToString();

            this.Write();
            this.DoSettingChanged(name, value);
        }

        protected void DoSettingChanged(string name, object value)
        {
            if (this.SettingChanged != null)
            {
                this.eventArgs.Name = name;
                this.eventArgs.Value = value;
                this.SettingChanged(this, this.eventArgs);
                this.eventArgs.Value = null;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Reads values into the <see cref="dataStore"/> filed using linq.
        /// </summary>
        /// <exception cref="FileNotFoundException">
        /// </exception>
        /// <exception cref="FileLoadException">
        /// </exception>
        private void Read()
        {
            // only update reading settings file every 5 seconds
            if (DateTime.Now < this.lastReadTime + TimeSpan.FromSeconds(this.readDelayInSeconds))
            {
                return;
            }

            if (!File.Exists(this.FileName))
            {
                throw new FileNotFoundException("Could not find settings file.", this.FileName);
            }

            var writeTime = File.GetLastWriteTime(this.FileName);

            // check if the file has been written to since last read attempt
            if (writeTime <= this.lastWriteTime)
            {
                XDocument doc = XDocument.Load(this.FileName);
                if (doc.Root.Name != "settings")
                {
                    throw new FileLoadException("Settings file root node is not 'settings'!");
                }

                var results = from x in doc.Root.Elements("entry")
                              where x.HasAttributes
                              let key = x.Attribute("key")
                              where key != null && !string.IsNullOrEmpty(key.Value)
                              select new { Key = key.Value, x.Value };
                this.dataStore = results.ToDictionary(k => k.Key, v => v.Value);

                this.lastWriteTime = writeTime;
                this.lastReadTime = DateTime.Now;
            }
        }

        /// <summary>
        /// Saves current values in the <see cref="dataStore"/> to a xml file using linq.
        /// </summary>
        private void Write()
        {
            var directoryName = Path.GetDirectoryName(this.FileName);
            if (!Directory.Exists(directoryName))
            {
                Directory.CreateDirectory(directoryName);
            }

            var doc = new XDocument(
                new XDeclaration("1.0", "UTF-8", "true"), 
                new XElement(
                    "settings", this.dataStore.Select(x => new XElement("entry", new XAttribute("Key", x.Key), x.Value))));
            doc.Save(this.FileName);

            this.lastWriteTime = File.GetLastWriteTime(this.FileName);
        }

        #endregion
    }
}