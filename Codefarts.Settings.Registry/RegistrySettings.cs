﻿using System.ComponentModel.Composition;
using System.Reflection;
using Microsoft.Win32;

namespace CBX.Settings.Registry
{
    [Export(typeof(ISettings))]
    public class RegistrySettings : ISettings
    {
        public T GetSetting<T>(string name)
        {
            var reg = Microsoft.Win32.Registry.CurrentUser;
            var appName = Assembly.GetEntryAssembly().GetName().Name;

            var key = reg.OpenSubKey(string.Format("Software\\{0}", appName));
            if (key == null) return default(T);
            var data = key.GetValue(name, default(T));
            return (T)data;
        }

        public void SetSetting(string name, object value)
        {
            var reg = Microsoft.Win32.Registry.CurrentUser;
            var appName = Assembly.GetEntryAssembly().GetName().Name;

            var key = reg.OpenSubKey(string.Format("Software\\{0}", appName), true) ??
                            reg.CreateSubKey(string.Format("Software\\{0}", appName), RegistryKeyPermissionCheck.ReadWriteSubTree);
            if (key != null) key.SetValue(name, value);
        }
    }
}
