﻿using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;

namespace CBX.Settings.Json
{
    using System;
    using System.Collections.Generic;

    public class JsonSettings
    {
        public JsonSettings(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) throw new ArgumentNullException("fileName");
            this.FileName = fileName;
        }

        public string FileName { get; private set; }
        private DateTime lastWriteTime = DateTime.MinValue;
        private Dictionary<string, string> dataStore = new Dictionary<string, string>();

        private void Read()
        {
            if (!System.IO.File.Exists(this.FileName)) throw new System.IO.FileNotFoundException("Could not find language file.", this.FileName);
          
            var writeTime = System.IO.File.GetLastWriteTime(this.FileName);
            if (writeTime > lastWriteTime)
            {
                 
                                                                                  throw  new NotImplementedException();
                //var doc = XDocument.Load(this.FileName);
                //var results = from x in doc.Root.Elements("entry")
                //              where x.HasAttributes
                //              let key = x.Attribute("key")
                //              where key != null && !string.IsNullOrEmpty(key.Value)
                //              select new { Key = key.Value, x.Value };
                //this.dataStore = results.ToDictionary(k => k.Key, v => v.Value);

                this.lastWriteTime = writeTime;
            }
        }

        private void Write()
        {
            var directoryName = System.IO.Path.GetDirectoryName(this.FileName);
            if (!System.IO.Directory.Exists(directoryName)) System.IO.Directory.CreateDirectory(directoryName);

                                      throw new NotImplementedException();
            //var doc = new XDocument(new XDeclaration("1.0", "UTF-8", "true"),
            //                        new XElement("settings",
            //                                     this.dataStore.Select(x => new XElement("entry", new XAttribute("Key", x.Key), x.Value))
            //                        ));
            //doc.Save(this.FileName);

            this.lastWriteTime = System.IO.File.GetLastWriteTime(this.FileName);
        }

        public T GetSetting<T>(string name)
        {
            throw new NotImplementedException();
            var type = typeof(T);
            if (type != typeof(string)) throw new ArgumentException("Generic type T must be of type string.");
            this.Read();
            return (T)Convert.ChangeType(this.dataStore[name], type);
        }

        public void SetSetting(string name, object value)
        {
            throw new NotImplementedException();
            if (!(value is string)) throw new ArgumentException("'value' argument must be of type string.");

            if (!this.dataStore.ContainsKey(name))   this.dataStore.Add(name, null);
                this.dataStore[name] = value.ToString();

            this.Write();
        }  }
}
