﻿/*
<copyright>
  Copyright (c) 2012, Dean Lunz
  All rights reserved.
  deanlunz@createdbyx.com
  http://www.createdbyx.com
</copyright>
*/
namespace CBX.Settings
{
    using System;                           

    /// <summary>
    /// provides extension methods for the <see cref="ISettings"/> type.
    /// </summary>
    public static class SettingsExtensionMethods
    {
        /// <summary>
        /// Tries to get a setting and if it fails will return the default value.
        /// </summary>
        /// <param name="settings">
        /// Reference to a <see cref="ISettings"/> type.
        /// </param>
        /// <param name="name">
        /// The name of the setting to get the value of.
        /// </param>
        /// <param name="defaultValue">
        /// The default value to return if there was a problem getting the setting.
        /// </param>
        /// <typeparam name="T">
        /// Defines the type that will be returned
        /// </typeparam>
        /// <returns>
        /// Returns the value of the setting or the default value if there was a problem retrieving the setting.
        /// </returns>
        public static T GetSetting<T>(this ISettings settings, string name, T defaultValue)
        {
            T value;
            if (TryGetSetting(settings, name, out value))
            {
                return value;
            }

            return defaultValue;
        }

        /// <summary>
        /// Tries to get the setting value.
        /// </summary>
        /// <param name="settings">
        /// Reference to the <see cref="ISettings"/> implementation.
        /// </param>
        /// <param name="name">
        /// The name of the setting.
        /// </param>
        /// <param name="value">
        /// The value of the setting if successful.
        /// </param>
        /// <typeparam name="T">
        /// The type of setting to retrieve.
        /// </typeparam>
        /// <returns>
        /// Returns true if the setting was retrieved successfully. Will return false if the setting was missing or threw an exception.
        /// </returns>
        public static bool TryGetSetting<T>(this ISettings settings, string name, out T value)
        {
            if (!settings.HasSetting(name))
            {
                value = default(T);
                return false;
            }

            T retrievedValue;
            try
            {
                retrievedValue = settings.GetSetting<T>(name);
            }
            catch (Exception)
            {
                value = default(T);
                return false;
            }

            value = retrievedValue;
            return true;
        }
    }
}
