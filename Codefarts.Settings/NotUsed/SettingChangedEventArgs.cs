/*
<copyright>
  Copyright (c) 2012, Dean Lunz
  All rights reserved.
  deanlunz@createdbyx.com
  http://www.createdbyx.com
</copyright>
*/
namespace CBX.Settings
{
    public class SettingChangedEventArgs :System.EventArgs
    {
        /// <summary>
        /// Gets the name of the setting that changed.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the value of the setting.
        /// </summary>
        public object Value { get; set; }
    }
}