﻿/*
<copyright>
  Copyright (c) 2012, Dean Lunz
  All rights reserved.
  deanlunz@createdbyx.com
  http://www.createdbyx.com
</copyright>
*/


namespace CBX.Settings
{
    using System;

#if USEMEF
    using System;
using System.Collections.ObjectModel;
    using System.ComponentModel.Composition;
    using System.ComponentModel.Composition.Hosting;
    using System.Diagnostics;
#endif

    public sealed class SettingsManager : ISettings
    {
        private static SettingsManager settingsManager;



        public event EventHandler<SettingChangedEventArgs> SettingChanged;

#if USEMEF
      public ObservableCollection<string> SearchFolders { get; set; }
  
        [Import(typeof(ISettings))] 
#endif
        private ISettings settings;

        private SettingChangedEventArgs eventArgs = new SettingChangedEventArgs();

        private SettingsManager()
        {
#if USEMEF
              this.SearchFolders = new ObservableCollection<string>();
#endif
        }

        public static SettingsManager Instance
        {
            get
            {
                if (settingsManager == null)
                {
                    settingsManager = new SettingsManager();
                    var manifestFilter = System.Globalization.CultureInfo.CurrentCulture.Name;
                    manifestFilter = System.IO.Path.ChangeExtension(manifestFilter, ".txt");
                    Localization.LocalizationHelpers.RegisterTextFromManifest(typeof(SettingsManager).Assembly, manifestFilter);
                }

                return settingsManager;
            }
        }


        public ISettings Settings
        {
            get
            {
#if USEMEF
                if (this.settings == null)
                {
                    try
                    {
                        // setup composition container
                        var catalog = new AggregateCatalog();

                        foreach (var folder in this.SearchFolders)
                        {
                            // check if folder exists
                            if (System.IO.Directory.Exists(folder))
                            {
                                catalog.Catalogs.Add(new DirectoryCatalog(folder, "*.dll"));
                            }
                        }

                        // compose and create plug ins
                        var composer = new CompositionContainer(catalog);
                        composer.ComposeParts(this);
                    }
                    catch (Exception ex)
                    {
                        // do something here
                        Debug.Write(ex, "Error");
                    }
                } 
#endif
                return this.settings;
            }

            set
            {
                if (this.settings != null)
                {
                    this.settings.SettingChanged -= settings_SettingChanged;
                }

                this.settings = value;

                if (this.settings != null)
                {
                    this.settings.SettingChanged += settings_SettingChanged;
                }
            }
        }

        void settings_SettingChanged(object sender, SettingChangedEventArgs e)
        {
            this.DoSettingChanged(e.Name, e.Value);
        }

        public T GetSetting<T>(string name)
        {
            if (this.settings == null)
            {
                var local = Localization.LocalizationManager.Instance;
                throw new NullReferenceException(local.Get("ERR_SettingsPropNotSet10245"));
            }

            return this.settings.GetSetting<T>(name);
        }

        public void RemoveSetting(string name)
        {
            this.settings.RemoveSetting(name);
        }

        public void SetSetting(string name, object value)
        {
            if (this.settings == null)
            {
                var local = Localization.LocalizationManager.Instance;
                throw new NullReferenceException(local.Get("ERR_SettingsPropNotSet10245"));
            }

            this.settings.SetSetting(name, value);
            this.DoSettingChanged(name, value);
        }

        private void DoSettingChanged(string name, object value)
        {
            if (this.SettingChanged != null)
            {
                this.eventArgs.Name = name;
                this.eventArgs.Value = value;
                this.SettingChanged(this, this.eventArgs);
                this.eventArgs.Value = null;
            }
        }

        public bool HasSetting(string name)
        {
            if (this.settings == null)
            {
                //  var manager = Localization.LocalizationManager.Instance;
                return false;
                //throw new NullReferenceException(manager.Get("ERR_SettingsPropNotSet"));
            }

            return this.settings.HasSetting(name);
        }

        public string[] GetSettingNames()
        {
            return this.settings.GetSettingNames();
        }
    }
}