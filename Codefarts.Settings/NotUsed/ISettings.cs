namespace CBX.Settings
{
    public interface ISettings
    {
           T GetSetting<T>(string name);
        bool HasSetting(string name);
        void RemoveSetting(string name);
        void SetSetting(string name, object value);

        string[] GetSettingNames();

        event System.EventHandler<SettingChangedEventArgs> SettingChanged;
    }
}