/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.Settings
{
    /// <summary>
    /// Provides event arguments for the <see cref="IValues{TKey}"/> interface.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ValueChangedEventArgs<T> :System.EventArgs
    {
        /// <summary>
        /// Gets the key of the setting that changed.
        /// </summary>
        public T Name { get;   set; }

        /// <summary>
        /// Gets the value of the setting.
        /// </summary>
        public object Value { get;   set; }
    }
}