﻿using System.ComponentModel.Composition;

namespace CBX.Settings.ASP
{
    using System;

    [Export(typeof(ISettings))]
    public class ASPSettings  :ISettings
    {
        public T GetSetting<T>(string name)
        {
            var type = typeof(T);
            if (type != typeof(string)) throw new ArgumentException("Generic type T must be of type string.");
            var sett = System.Web.Configuration.WebConfigurationManager.AppSettings;
            var value = sett[name];
            if (value.StartsWith("~/")) value = System.Web.HttpContext.Current.Server.MapPath(value);
            return (T) Convert.ChangeType(value, type) ;
        }

        public void SetSetting(string name, object value)
        {
            if(!(value is string)) throw  new ArgumentException("'value' argument must be of type string.");
            var sett = System.Web.Configuration.WebConfigurationManager.AppSettings;
            sett.Set(name, value as string);
        }
    }
}
